package com.nortal.race.assignment.controller;

import java.awt.*;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.nortal.race.assignment.model.*;

public class RaceController {
    private int i = 0;
//    private static int countDown = 0;

    public RaceController() {
    }

    private List<Point> targetsToCapture;

    public VesselCommand calculateNextCommand(Vessel vessel, RaceArea raceArea) {
        double targetLocX = 0;
        double targetLocY = 0;
        double vesselLocX = 0;
        double vesselLocY = 0;
        double vesselSpeedX = 0;
        double vesselSpeedY = 0;

        double brakingDistance = 10;
        double brakingSpeed = 5;

        Thruster thruster = null;
        ThrusterLevel thrusterLvl = null;

        targetsToCapture = raceArea.getTargets();
        Collections.sort(targetsToCapture, new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2){ return Double.compare(o1.getY(), o2.getY());}

        });

        targetLocX = targetsToCapture.get(i).getX();
        targetLocY = targetsToCapture.get(i).getY();
        vesselLocX = vessel.getPosition().getX();
        vesselLocY = vessel.getPosition().getY();
        vesselSpeedX = vessel.getSpeedX();
        vesselSpeedY = vessel.getSpeedY();

    if (vesselSpeedX >= Math.abs(brakingSpeed) && vesselLocX >= targetLocX - brakingDistance && vesselLocX <= targetLocX + brakingDistance){
        if(Math.signum(vesselSpeedX) == 1){
            thruster = Thruster.RIGHT;
            thrusterLvl = ThrusterLevel.T4;
        }
        else{
            thruster = Thruster.LEFT;
            thrusterLvl = ThrusterLevel.T4;
        }
    }
    else if(vesselSpeedY >= Math.abs(brakingSpeed) && vesselLocY >= targetLocY - brakingDistance && vesselLocY <= targetLocY + brakingDistance){
        if(Math.signum(vesselSpeedY) == 1){
            thruster = Thruster.FRONT;
            thrusterLvl = ThrusterLevel.T4;
        }
        else{
            thruster = Thruster.BACK;
            thrusterLvl = ThrusterLevel.T4;
        }
    }
    else if (vesselLocY >= targetLocY+2 || vesselLocY <= targetLocY-2) {

        if (vesselLocY < targetLocY - targetLocY / 2) {
            thruster = Thruster.BACK;
            thrusterLvl = ThrusterLevel.T4;
        } else if (vesselLocY < targetLocY - targetLocY / 8) {
            thruster = Thruster.BACK;
            thrusterLvl = ThrusterLevel.T3;
        } else if (vesselLocY < targetLocY - 1) {
            thruster = Thruster.BACK;
            thrusterLvl = ThrusterLevel.T2;
        } else if (vesselLocY > targetLocY + targetLocY / 2) {
            thruster = Thruster.FRONT;
            thrusterLvl = ThrusterLevel.T4;
        } else if (vesselLocY > targetLocY + targetLocY / 8) {
            thruster = Thruster.FRONT;
            thrusterLvl = ThrusterLevel.T3;
        } else if (vesselLocY > targetLocY + 1) {
            thruster = Thruster.FRONT;
            thrusterLvl = ThrusterLevel.T2;
        } else {
            thruster = Thruster.FRONT;
            thrusterLvl = ThrusterLevel.T0;
        }
    }
        else{
            if (vesselLocX < targetLocX - targetLocX / 2) {
                thruster = Thruster.LEFT;
                thrusterLvl = ThrusterLevel.T4;
            } else if (vesselLocX < targetLocX - targetLocX / 8) {
                thruster = Thruster.LEFT;
                thrusterLvl = ThrusterLevel.T3;
            } else if (vesselLocX < targetLocX - 1) {
                thruster = Thruster.LEFT;
                thrusterLvl = ThrusterLevel.T2;
            } else if (vesselLocX > targetLocX + targetLocX / 2) {
                thruster = Thruster.RIGHT;
                thrusterLvl = ThrusterLevel.T4;
            } else if (vesselLocX > targetLocX + targetLocX / 8) {
                thruster = Thruster.RIGHT;
                thrusterLvl = ThrusterLevel.T3;
            } else if (vesselLocX > targetLocX + 1) {
                thruster = Thruster.RIGHT;
                thrusterLvl = ThrusterLevel.T2;
            } else {
                thruster = Thruster.RIGHT;
                thrusterLvl = ThrusterLevel.T0;
            }
        }
        /*//Developed an equation that calculates at what distance to release the thruster to arrive exactly
        at the given location. Proved to be useless as the timestep is too infrequent.

        double Thrust = 2.4;
        double water = 1.6;

        double dropThrustX = (water*targetLoc.getX())/(water+Thrust);
        double dropThrustY = (water*targetLoc.getY())/(water+Thrust);

       */
        if (vesselLocX < targetLocX+2 && vesselLocX > targetLocX-2 && vesselLocY < targetLocY+2 && vesselLocY > targetLocY-2 && i < targetsToCapture.size()-1)
        {
            i++;
        }


        VesselCommand vesselCommand = new VesselCommand(thruster, thrusterLvl);

        // TODO: Implement algorithm to return command for the vessel to capture all the targets on the raceArea provided
        return vesselCommand;
    }



}

